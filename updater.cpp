#include "updater.h"

#include <QtNetwork/QNetworkAccessManager>
#include <QRegularExpression>
#include <QCoreApplication>
#include <QDebug>
#include <QStringList>
#include <QCryptographicHash>
#include <QTimer>

Updater::Updater(QString filename, QObject *parent) :
  QObject(parent),
  m_feedsLeft(0)
{
  m_manager = new QNetworkAccessManager(this);
  connect(m_manager, &QNetworkAccessManager::finished,
          this, &Updater::replyFinished);
  m_file.setFileName(filename);
}

Updater::~Updater()
{
  QTextStream out(&m_file);
  out << R"(</channel></rss>)";
  m_file.close();
  delete m_manager;
}

void Updater::fetchFeeds(QList<Feed> feeds)
{
  if (m_file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream out(&m_file);
    out << R"(<?xml version="1.0" encoding="UTF-8"?><rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">)";
    out << R"(<channel><title>Comics</title>)";

//    int i = 0;
    m_feedsLeft = feeds.size();
    for (Feed feed : feeds)
    {
//      QTimer::singleShot(i++ * 1000, [=]{
        qWarning() << "Updating feed" << feed.url;
        QNetworkRequest request(feed.url);
        request.setRawHeader("User-Agent" , "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0");
        m_requests[m_manager->get(request)] = feed;
//      });
//      qWarning() << "Updating feed" << feed.url;
    }
  }
  else
    qWarning() << "file eror" << m_file.errorString();

  QTimer::singleShot(300000, [](){ QCoreApplication::quit(); });
}

void Updater::replyFinished(QNetworkReply *reply)
{
  qWarning() << "Got reply on url" << reply->url() << reply->header(QNetworkRequest::LastModifiedHeader);
  Feed feed = m_requests.take(reply);
  QUrl redirect = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();

  if (redirect.isValid())
  {
    QNetworkRequest request(redirect);
    request.setRawHeader("User-Agent" , "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0");
    m_requests[m_manager->get(request)] = feed;
    return;
  }

  feed.itemRegex.setPatternOptions(QRegularExpression::DotMatchesEverythingOption);
  feed.itemContentRegex.setPatternOptions(QRegularExpression::DotMatchesEverythingOption);
  feed.itemLinkRegex.setPatternOptions(QRegularExpression::DotMatchesEverythingOption);
  feed.itemDateRegex.setPatternOptions(QRegularExpression::DotMatchesEverythingOption);
  feed.itemTitleRegex.setPatternOptions(QRegularExpression::DotMatchesEverythingOption);
  feed.redirectUrl.setPatternOptions(QRegularExpression::DotMatchesEverythingOption);

  QString str(reply->readAll());

  if (!feed.redirectUrl.pattern().isEmpty() && reply->url() == feed.url)
  {
    qWarning() << "Manual redirect";
    QRegularExpressionMatch rMatch = feed.redirectUrl.match(str);

    if (rMatch.hasMatch())
    {
//      qWarning() << "Match" << rMatch.capturedTexts();
      QUrl redirectUrl = QUrl(rMatch.captured(1));

      if (!redirectUrl.isEmpty())
      {
        QNetworkRequest request(redirectUrl);
        request.setRawHeader("User-Agent" , "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0");
        m_requests[m_manager->get(request)] = feed;
      }
    }
    else
      qWarning() << "Empty redirect url";

    return;
  }

//  qWarning() << str;
  QRegularExpressionMatchIterator i = feed.itemRegex.globalMatch(str);

  QTextStream out(&m_file);

  int count = 0;
  while (i.hasNext() && (count++ < feed.itemLimit || feed.itemLimit <= 0)) {
    QRegularExpressionMatch itemMatch = i.next();

    const QString item = itemMatch.captured(1);

    if (!item.isNull())
    {
      QString result("<item>\n<title><![CDATA[%1]]></title>\n<link>%2</link>\n<pubDate>%3</pubDate>\n<content:encoded><![CDATA[%4]]></content:encoded>\n<guid isPermaLink=\"false\">%5</guid>\n</item>\n");

      QStringList errors;

      QRegularExpressionMatch titleMatch = feed.itemTitleRegex.match(item);

      QString title(feed.itemTitle);
      int len = titleMatch.capturedTexts().length();
      if (title.contains("%") && len <= 1)
        errors << "Title regex fail: " + feed.itemTitleRegex.pattern();
      for(int i = 1; i < len; i++)
      {
        title = title.arg(titleMatch.captured(i));
        qWarning() << "title" << i << titleMatch.captured(i);
      }
//      qDebug() << feed.itemTitleRegex.pattern() << titleMatch.capturedTexts();


      QRegularExpressionMatch linkMatch = feed.itemLinkRegex.match(item);

      QString link(feed.itemLink);
      len = linkMatch.capturedTexts().length();
      if (link.contains("%") && len <= 1)
        errors << "Link regex fail: " + feed.itemLinkRegex.pattern();
      for(int i = 1; i < len; i++)
      {
        link = link.arg(linkMatch.captured(i));
        qWarning() << "link" << i << linkMatch.captured(i);
      }


      QRegularExpressionMatch dateMatch = feed.itemDateRegex.match(item);

      QString date(feed.itemDate);
      len = dateMatch.capturedTexts().length();
      if (date.contains("%") && len <= 1)
        errors << "Date regex fail: " + feed.itemDateRegex.pattern();
      for(int i = 1; i < len; i++)
      {
        date = date.arg(dateMatch.captured(i));
        qWarning() << "date" << i << dateMatch.captured(i);
      }


      QRegularExpressionMatch contentMatch = feed.itemContentRegex.match(item);

      QString content(feed.itemContent);
      len = contentMatch.capturedTexts().length();
      if (content.contains("%") && len <= 1)
        errors << "Content regex fail: " + feed.itemContentRegex.pattern();
      for(int i = 1; i < len; i++)
      {
        content = content.arg(contentMatch.captured(i));
        qWarning() << "content" << i << contentMatch.captured(i);
      }
//      qDebug() << feed.itemContentRegex.pattern() << contentMatch.capturedTexts();

      if (errors.isEmpty())
      {
        result = result.arg(title.isEmpty() ? "" : title);
        result = result.arg(link.isEmpty() ? reply->url().toString() : link);
        result = result.arg(date.isEmpty() ? "" : date);
        result = result.arg(content.isEmpty() ? "No content?!" : content);
      }
      else
      {
        result = result.arg("Error in feed: " + feed.url.toString());
        result = result.arg(reply->url().toString());
        result = result.arg("");
        result = result.arg(errors.join("<br />"));
        qWarning() << errors;
      }

      result = result.arg(QString(QCryptographicHash::hash(content.toUtf8(), QCryptographicHash::Md5).toHex()));

      out << result;
    }
    else
      qWarning() << feed.url << "Badly designed item match";

    //      int len = itemMatch.capturedTexts().length();
    //      QString s(feed.item);
    //      for(int i = 1; i < len; i++)
    //      {
    //        s = s.arg(itemMatch.captured(i));
    //        qWarning() << i << itemMatch.captured(i);
    //      }
    ////      qWarning() << s;
    //      out << s;
  }

  if (count <= 0)
  {
    QString result("<item>\n<title><![CDATA[%1]]></title>\n<link>%2</link>\n<pubDate>%3</pubDate>\n<content:encoded><![CDATA[%4]]></content:encoded>\n<guid isPermaLink=\"false\">%5</guid>\n</item>\n");

    result = result.arg("No items in feed: " + feed.url.toString());
    result = result.arg(reply->url().toString());
    result = result.arg("");
    result = result.arg(str);

    out << result;
  }

  qDebug() << "feeds left:" << m_feedsLeft;

  if (--m_feedsLeft <= 0)
    QCoreApplication::quit();
}
