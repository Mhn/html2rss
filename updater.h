#ifndef UPDATER_H
#define UPDATER_H

#include <QObject>
#include <QNetworkReply>
#include <QUrl>
#include <QRegularExpression>
#include <QFile>
#include <QList>
#include <QMap>

struct Feed {
  QUrl url;
  QRegularExpression redirectUrl;
  QRegularExpression itemRegex;
  QRegularExpression itemContentRegex;
  QString itemContent;
  QRegularExpression itemTitleRegex;
  QString itemTitle;
  QRegularExpression itemDateRegex;
  QString itemDate;
  QRegularExpression itemLinkRegex;
  QString itemLink;
  int itemLimit;
};

class QNetworkAccessManager;

class Updater : public QObject
{
  Q_OBJECT
public:
  explicit Updater(QString filename, QObject *parent = 0);
  ~Updater();

  void fetchFeeds(QList<Feed> feeds);

signals:

public slots:
  void replyFinished(QNetworkReply*reply);

private:
  QNetworkAccessManager *m_manager;
  QMap<QNetworkReply*, Feed> m_requests;
  QFile m_file;
  int m_feedsLeft;
};

#endif // UPDATER_H
