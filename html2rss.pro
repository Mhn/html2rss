#-------------------------------------------------
#
# Project created by QtCreator 2015-04-01T19:50:55
#
#-------------------------------------------------

QT       += core network
QT       -= gui
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
TARGET = html2rss
TEMPLATE = app


SOURCES += main.cpp \
    updater.cpp

HEADERS  += \
    updater.h

DISTFILES += \
    feeds.json \
    oldFeeds.json \
    newFeeds.json
