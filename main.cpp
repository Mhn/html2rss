#include <QCoreApplication>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

#include "updater.h"

QList<Feed> parseFile(QString filename)
{
  QFile file(filename);
  QList<Feed> feeds;
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    QByteArray data = file.readAll();
    file.close();
    qWarning() << "read fiel";

    QJsonDocument doc(QJsonDocument::fromJson(data));

//    QJsonObject json = doc.object();

    QJsonArray fs = doc.array();

    for (const QJsonValue &feedVal : fs)
    {
      qWarning() << "got stuff";
      QJsonObject f = feedVal.toObject();
      feeds << Feed {
        QUrl(f["url"].toString()),
        QRegularExpression(f["redirectUrl"].toString()),
        QRegularExpression(f["itemRegex"].toString()),
        QRegularExpression(f["itemContentRegex"].toString()),
        f["itemContent"].toString(),
        QRegularExpression(f["itemTitleRegex"].toString()),
        f["itemTitle"].toString(),
        QRegularExpression(f["itemDateRegex"].toString()),
        f["itemDate"].toString(),
        QRegularExpression(f["itemLinkRegex"].toString()),
        f["itemLink"].toString(),
        f["itemLimit"].toInt(2)
      };
    }
  }
  else
    qWarning() << "File error" << file.errorString();
  return feeds;
}

int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);

  if (argc > 2)
  {
    QList<Feed> feeds = parseFile(argv[1]);

    Updater u(argv[2]);
    u.fetchFeeds(feeds);
    return a.exec();
  }
  else
  {
    qWarning() << "Usage: html2rss feeds.json rss.xml";
  }
}
